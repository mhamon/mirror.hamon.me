#!/usr/bin/env bash

BASH_DIR="$(dirname "${BASH_SOURCE[0]}")"
if ! [ -f "$BASH_DIR/.env" ]; then
  echo "$BASH_DIR/.env not found"
  exit 1
fi

# shellcheck source=./.env
source "$BASH_DIR/.env"
if [ -z "$DATA_DIR" ]; then
  echo "DATA_DIR is empty"
  exit 1
fi

rootdir="$DATA_DIR"

target="${rootdir}/repo"
tmp="${rootdir}/tmp"
logdir="${rootdir}/log"
logname="sync"
logfile="${logdir}/${logname}-$(date +%Y%m%d).log"
lock="${rootdir}/syncrepo.lck"

bwlimit=0

source_url='rsync://rsync.cyberbits.eu/archlinux/'
lastupdate_url="${source_url}/lastupdate"

[ ! -d "${target}" ] && mkdir -p "${target}"
[ ! -d "${tmp}" ] && mkdir -p "${tmp}"
[ ! -d "${logdir}" ] && mkdir -p "${logdir}"

exec 9> "${lock}"
flock -n 9 || exit

_echo() {

  echo "$(date +%Y/%m/%d) $(date +%H:%M) :: $*"

}

rsync_cmd() {
  local -a cmd=(rsync -rtlH --safe-links --delete-after "--timeout=600" "--contimeout=60" -p --delay-updates --no-motd "--temp-dir=${tmp}")

  if stty &> /dev/null; then
    cmd+=(-h -v --progress)
  else
    cmd+=(--quiet)
  fi

  if ((bwlimit > 0)); then
    cmd+=("--bwlimit=$bwlimit")
  fi

  "${cmd[@]}" "$@"
}

log_rotate() {

  local _to_archive _to_remove
  _to_archive="$(find "$logdir" -name "*${logname}*.log" -mtime +3 | wc -l)"
  _to_remove="$(find "$logdir" -name "*${logname}*.log.gz" -mtime +30 | wc -l)"
  _echo "= Log rotation" >> "$logfile"
  [ "$_to_archive" -gt 0 ] && find "$logdir" -name "*${logname}*.log" -mtime +3 -print0 | xargs -0 gzip -v >> "$logfile" 2>&1
  [ "$_to_remove" -gt 0 ] && find "$logdir" -name "*${logname}*.log.gz" -mtime +30 -print0 | xargs -0 rm -v >> "$logfile" 2>&1

}

end() {

  _echo "=== End sync" >> "$logfile"
  echo >> "$logfile"
  exit 0

}

_echo "=== Start sync" >> "$logfile"

# if we are called without a tty (cronjob) only run when there are changes
if ! tty -s && [[ -f "$target/lastupdate" ]] && diff -b <(curl -Ls "$lastupdate_url") "$target/lastupdate" > /dev/null; then
  # keep lastsync file in sync for statistics generated by the Arch Linux website
  rsync_cmd "$source_url/lastsync" "$target/lastsync" >> "$logfile"
  end
fi

rsync_cmd --exclude='*.links.tar.gz*' --exclude='/other' --exclude='/sources' "${source_url}" "${target}" >> "$logfile"
log_rotate
end
