# Mirror Archlinux

Création et synchronisation d'un repository Archlinux perso.

## Docker
Le repository est exposé via une simple image nginx avec une configuration très simple. L'image est buildé par Gitlab CI.
Le docker-compose.yml et le .env permettent de lancer le conteneur avec la configuration Traefik nécessaire

## Scripts
 * sync_repo.sh : pour synchro le repository, à mettre en cron
```bash
# Sync mirror
0 12 * * * /srv/mirror.hamon.me/sync_repo.sh
```

